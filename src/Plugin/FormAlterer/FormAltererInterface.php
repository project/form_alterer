<?php

declare(strict_types=1);

namespace Drupal\form_alterer\Plugin\FormAlterer;

use Drupal\Component\Plugin\PluginInspectionInterface;
use Drupal\Core\Form\FormStateInterface;

/**
 * Interface for form_alterer plugins.
 */
interface FormAltererInterface extends PluginInspectionInterface {

  /**
   * Returns the form ids for the form_alterer plugin.
   */
  public function getFormIds(): array;

  /**
   * The global static overrides for the form(s).
   */
  public function getGlobalStaticOverrides(): array;

  /**
   * The specific static overrides for individual forms, keyed by form id.
   */
  public function getSpecificStaticOverrides(): array;

  /**
   * Alters the forms with the given form ids.
   */
  public function alterForm(&$form, FormStateInterface $form_state, $form_id);

  /**
   * Executes the form alteration with the private executeAlterForm() method.
   */
  public function execute(&$form, FormStateInterface $form_state, $form_id): void;

  /**
   * A flag for whether the form alterer has executed.
   */
  public function hasExecuted(): bool;

}
