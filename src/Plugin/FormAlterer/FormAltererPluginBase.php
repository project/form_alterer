<?php

declare(strict_types=1);

namespace Drupal\form_alterer\Plugin\FormAlterer;

use Drupal\Core\Plugin\PluginBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Component\Utility\NestedArray;

/**
 * Base class for form_alterer plugins.
 */
abstract class FormAltererPluginBase extends PluginBase implements FormAltererInterface {

  /**
   * {@inheritDoc}
   */
  public function alterForm(&$form, FormStateInterface $form_state, $form_id) {}

  /**
   * Executes the form alter method internally to perform needed logic.
   */
  private final function executeAlterForm(&$form, FormStateInterface $form_state, $form_id) {
    $form = $this->processGlobalStaticOverridesForm($form, $form_state, $form_id);
    $form = $this->processSpecificStaticOverridesForm($form, $form_state, $form_id);
    if (!isset($form['#cache'])) {
      $form['#cache'] = [];
    }
    if (!in_array('form_alterer_plugins', $form['#cache'])) {
      $form['#cache'][] = 'form_alterer_plugins';
    }
    $this->alterForm($form, $form_state, $form_id);
  }

  /**
   * {@inheritDoc}
   */
  public function execute(&$form, FormStateInterface $form_state, $form_id): void {
    $this->executeAlterForm($form, $form_state, $form_id);
    $this->configuration['has_executed'] = TRUE;
  }

  /**
   * {@inheritDoc}
   */
  public function getFormIds(): array {
    return $this->pluginDefinition['form_ids'];
  }

  /**
   * {@inheritDoc}
   */
  public function hasExecuted(): bool {
    return $this->configuration['has_executed'] ?? FALSE;
  }

  /**
   * {@inheritDoc}
   */
  public function getGlobalStaticOverrides(): array {
    return $this->pluginDefinition['static_overrides'];
  }

  /**
   * {@inheritDoc}
   */
  public function getSpecificStaticOverrides(): array {
    return $this->pluginDefinition['specific_static_overrides'];
  }

  /**
   * Processes the global static overrides, if any, for the form alterer plugin.
   */
  public function processGlobalStaticOverridesForm($form, FormStateInterface $form_state, $form_id) {
    if (!empty($this->getGlobalStaticOverrides())) {
      $form = NestedArray::mergeDeep(
        $form,
        $this->getGlobalStaticOverrides(),
      );
    }
    return $form;
  }

  /**
   * Processes the specific static overrides, if any, for the form alterer plugin.
   */
  public function processSpecificStaticOverridesForm($form, FormStateInterface $form_state, $form_id) {
    $form_id = $form_state->getFormObject()->getFormId();
    if (!empty($this->getSpecificStaticOverrides())) {
      foreach ($this->getSpecificStaticOverrides() as $this_form_id => $overrides) {
        if ($form_id !== $this_form_id) {
          continue;
        }
        $form = NestedArray::mergeDeep(
          $form,
          $overrides,
        );
      }
    }
    return $form;
  }

}
