<?php

declare(strict_types=1);

namespace Drupal\form_alterer\Services;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Component\Plugin\PluginManagerInterface;

/**
 * FormAlterer plugin manager interface.
 */
interface FormAltererPluginManagerInterface extends PluginManagerInterface {

  /**
   * Gets all of the unique form ids associated with form alterer plugins.
   *
   * @return array
   *   Gets all of the form ids keyed by form id.
   */
  public function getAllFormIds(): array;

  /**
   * Returns all of the form alterer definitions for the form id given.
   *
   * @param string $form_id
   *   The form id or base form id.
   *
   * @return array
   *   Returns the form alterer definitions based on form id or base form id.
   */
  public function getDefinitionsFromFormId($form_id);

  /**
   * Returns all plugin instances based on the form id given.
   *
   * @return \Drupal\form_alterer\Plugin\FormAlterer\FormAltererInterface[]
   *   Returns all form alterer plugin instances based on the form id.
   */
  public function getInstancesFromFormId(string $form_id): array;

  /**
   * Returns all plugin instances based on the form and form state.
   *
   * @return \Drupal\form_alterer\Plugin\FormAlterer\FormAltererInterface[]
   *   Returns all form alterer plugin instances based on the form and form state.
   */
  public function getFormInstances($form, FormStateInterface $form_state);

  /**
   * Alters the given forms with form_alterer plugins.
   */
  public function alterForm(&$form, FormStateInterface $form_state, $form_id);

}
