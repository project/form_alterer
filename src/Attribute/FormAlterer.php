<?php

namespace Drupal\form_alterer\Attribute;

use Drupal\Component\Plugin\Attribute\Plugin;
use Drupal\Core\StringTranslation\TranslatableMarkup;

/**
 * Defines the FormAlterer plugin attribute object.
 */
#[\Attribute(\Attribute::TARGET_CLASS)]
class FormAlterer extends Plugin {

  /**
   * Constructs a FormAlterer 'form_alterer' attribute object.
   *
   * @param string $id
   *   (required) The form alterer id.
   * @param string[] $form_ids
   *   (required) The form alterer form_ids or base_form_ids.
   * @param mixed $label
   *   (optional) The form alterer label.
   * @param mixed $description
   *   (optional) The form alterer description.
   * @param array $static_overrides
   *   (optional) The static overrides for the form.
   * @param array $specific_static_overrides
   *   (optional) The specific overrides for each form keyed by one of the form_ids.
   * @param int $execution_order
   *   (optional) The execution order of the form alteration. Defaults to 1.
   * @param mixed $deriver
   *   (optional) The form alterer plugin deriver.
   */
  public function __construct(
    public readonly string $id,
    public readonly array $form_ids,
    public readonly ?TranslatableMarkup $label = NULL,
    public readonly ?TranslatableMarkup $description = NULL,
    public readonly array $static_overrides = [],
    public readonly array $specific_static_overrides = [],
    public readonly int $execution_order = 1,
    public readonly ?string $deriver = NULL,
  ) {}

}
