<?php

declare(strict_types=1);

namespace Drupal\form_alterer\Services;

use Drupal\Core\Form\FormCacheInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Access\CsrfTokenGenerator;
use Drupal\Core\Form\FormSubmitterInterface;
use Drupal\Core\Form\FormValidatorInterface;
use Drupal\Core\Theme\ThemeManagerInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Drupal\Core\Render\ElementInfoManagerInterface;
use Drupal\Core\Form\FormBuilder as CoreFormBuilder;
use Drupal\Core\DependencyInjection\ClassResolverInterface;
use Symfony\Contracts\EventDispatcher\EventDispatcherInterface;

/**
 * Overrides the form builder service to use form alterer plugins.
 *
 * The core form builder service alters forms using the hook_form_HOOK_alter methods for
 * form ids and base form ids. This new service allows users to define form_alterer PHP Attribute plugins
 * and YAML plugins for form alterations. The purpose of this override is to provide a more organized way
 * to alter many forms across multiple modules and/or themes.
 */
class FormBuilder extends CoreFormBuilder {

  /**
   * {@inheritDoc}
   *
   * Includes the plugin.manager.form_alterer service.
   */
  public function __construct(FormValidatorInterface $form_validator, FormSubmitterInterface $form_submitter, FormCacheInterface $form_cache, ModuleHandlerInterface $module_handler, EventDispatcherInterface $event_dispatcher, RequestStack $request_stack, ClassResolverInterface $class_resolver, ElementInfoManagerInterface $element_info, ThemeManagerInterface $theme_manager, private readonly FormAltererPluginManager $formAltererPluginManager, CsrfTokenGenerator|null $csrf_token = NULL) {
    parent::__construct($form_validator, $form_submitter, $form_cache, $module_handler, $event_dispatcher, $request_stack, $class_resolver, $element_info, $theme_manager, $csrf_token);
  }

  /**
   * {@inheritDoc}
   */
  public function prepareForm($form_id, &$form, FormStateInterface &$form_state) {
    parent::prepareForm($form_id, $form, $form_state);
    $this->formAltererPluginManager->alterForm($form, $form_state, $form_id);
  }

}
