# Form Alterer Module
The Form Alterer module provides OOP plugin functionality to developers for easily altering forms in Drupal 9+. Typically, forms are altered via the hook system (i.e. hook_form_alter) in a module's *.module file. However, this is tedious, overly repetitive, and it really doesn't follow Drupal's new best practices for OOP design. Now, we have a module where we can alter forms via the Drupal Plugin API using an object-oriented approach.

## Features
- Provides a new Annotation plugin that is discovered at
`Drupal\my_module\Plugin\FormAlterer`
- Provides a new YAML plugin that is discovered at `my_module.form_alterers.yml`
- Plugins can provide a list of form_ids in the Plugin Definition to alter forms in different contexts
- Plugins can provide a base form id in the list of form_ids to provide more general form altering
- Plugins can provide static_overrides in the Plugin Definition to alter the form(s) statically