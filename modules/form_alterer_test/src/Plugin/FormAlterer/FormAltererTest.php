<?php

namespace Drupal\form_alterer_test\Plugin\FormAlterer;
use Drupal\Core\Form\FormStateInterface;
use Drupal\form_alterer\Attribute\FormAlterer;
use Drupal\form_alterer\Plugin\FormAlterer\FormAltererPluginBase;

/**
 * The test form_alterer plugin class.
 */
#[FormAlterer(
  id: 'test_form_alter_attribute',
  form_ids: [
    'node_page_edit_form',
  ]
)]
class FormAltererTest extends FormAltererPluginBase {

  /**
   * {@inheritDoc}
   */
  public function alterForm(&$form, FormStateInterface $form_state, $form_id) {
    $form[$this->getPluginId()] = [
      '#type' => 'html_tag',
      '#tag' => 'p',
      '#value' => 'This is an alteration of the page edit form.',
    ];
  }

}
