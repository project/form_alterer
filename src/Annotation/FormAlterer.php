<?php

declare(strict_types=1);

namespace Drupal\form_alterer\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines form_alterer annotation object.
 *
 * @Annotation
 */
final class FormAlterer extends Plugin {

  /**
   * The form_alterer plugin ID.
   */
  public readonly string $id;

  /**
   * The human-readable name of the form_alterer plugin.
   *
   * @ingroup plugin_translatable
   */
  public readonly string $label;

  /**
   * The description of the form_alterer plugin.
   *
   * @ingroup plugin_translatable
   */
  public readonly string $description;

  /**
   * The form ids of the form_alterer plugin.
   */
  public readonly array $form_ids;

}
