<?php

declare(strict_types=1);

namespace Drupal\form_alterer\Services;

use Drupal\Component\Plugin\Discovery\AttributeBridgeDecorator;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Plugin\DefaultPluginManager;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\Discovery\YamlDiscoveryDecorator;
use Drupal\Core\Plugin\Discovery\AttributeClassDiscovery;
use Drupal\form_alterer\Plugin\FormAlterer\FormAltererDefault;
use Drupal\form_alterer\Plugin\FormAlterer\FormAltererInterface;
use Drupal\Core\Plugin\Discovery\AttributeDiscoveryWithAnnotations;
use Drupal\form_alterer\Attribute\FormAlterer as FormAltererAttribute;
use Drupal\Core\Plugin\Discovery\ContainerDerivativeDiscoveryDecorator;
use Drupal\form_alterer\Annotation\FormAlterer as FormAltererAnnotation;
use Drupal\Component\Annotation\Plugin\Discovery\AnnotationBridgeDecorator;
use Drupal\Core\Extension\ThemeHandlerInterface;

/**
 * FormAlterer plugin manager.
 */
final class FormAltererPluginManager extends DefaultPluginManager implements FormAltererPluginManagerInterface {

  /**
   * {@inheritDoc}
   */
  protected $defaults = [
    'static_overrides' => [],
    'specific_static_overrides' => [],
    'execution_order' => 1,
    'class' => FormAltererDefault::class,
    // Backwards compatibility.
    // @deprecated Drupal 11+.
    'category' => 'default',
  ];

  /**
   * The form alterer prototypes.
   *
   * @var \Drupal\form_alterer\Plugin\FormAlterer\FormAltererInterface[]
   */
  protected $prototypes = [];

  /**
   * All of the form ids associated with form alterer plugins.
   *
   * @var string[]
   */
  protected $formIds;

  /**
   * Constructs the object.
   */
  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler, private readonly ThemeHandlerInterface $themeHandler) {
    parent::__construct('Plugin/FormAlterer', $namespaces, $module_handler, FormAltererInterface::class, FormAltererAttribute::class, FormAltererAnnotation::class);
    $this->alterInfo('form_alterer_info');
    $this->setCacheBackend($cache_backend, 'form_alterer_plugins');
  }

  /**
   * {@inheritDoc}
   */
  protected function getDiscovery() {
    $has_annotation = isset($this->pluginDefinitionAnnotationName);
    $has_attribute = isset($this->pluginDefinitionAttributeName);
    if (!$this->discovery) {
      if ($has_attribute && $has_annotation) {
        $discovery = new AttributeDiscoveryWithAnnotations($this->subdir, $this->namespaces, $this->pluginDefinitionAttributeName, $this->pluginDefinitionAnnotationName, $this->additionalAnnotationNamespaces);
      }
      else {
        $discovery = new AttributeClassDiscovery($this->subdir, $this->namespaces, $this->pluginDefinitionAttributeName);
      }
      $yaml_directories = $this->themeHandler->getThemeDirectories() + $this->moduleHandler->getModuleDirectories();
      $discovery = new YamlDiscoveryDecorator($discovery, 'form_alterers', $yaml_directories);
      $discovery
        ->addTranslatableProperty('label')
        ->addTranslatableProperty('description')
        ->addTranslatableProperty('category');
      if ($has_annotation) {
        $discovery = new AnnotationBridgeDecorator($discovery, $this->pluginDefinitionAnnotationName);
      }

      $discovery = new ContainerDerivativeDiscoveryDecorator($discovery);
      $this->discovery = $discovery;
    }
    return $this->discovery;
  }

  /**
   * {@inheritDoc}
   */
  public function getAllFormIds(): array {
    $definitions = $this->getDefinitions();
    if (!isset($this->formIds)) {
      foreach ($definitions as $definition) {
        foreach ($definition['form_ids'] as $form_id) {
          $this->formIds[$form_id] = $form_id;
        }
      }
    }
    return $this->formIds;
  }

  /**
   * {@inheritDoc}
   */
  public function getDefinitionsFromFormId($form_id) {
    $definitions = array_filter($this->getDefinitions(), function ($definition) use ($form_id) {
      return in_array($form_id, $definition['form_ids']);
    });
    return $definitions;
  }

  /**
   * {@inheritDoc}
   */
  public function getInstancesFromFormId(string $form_id): array {
    $instances = [];
    foreach ($this->getDefinitionsFromFormId($form_id) as $id => $definition) {
      if (!isset($this->prototypes[$id])) {
        $this->prototypes[$id] = $this->createInstance($id);
      }
      $instances[$id] = $this->prototypes[$id];
    }
    return $instances;
  }

  /**
   * {@inheritDoc}
   */
  public function getFormInstances($form, FormStateInterface $form_state) {
    $base_form_id = $form_state->getBuildInfo()['base_form_id'] ?? NULL;
    $form_id = $form_state->getFormObject()->getFormId();
    $form_ids = [$form_id];
    if ($base_form_id) {
      $form_ids[] = $base_form_id;
    }
    $form_instances = [];
    // Check for instances with a form_id or form_id that is a base form id.
    foreach ($form_ids as $form_id) {
      foreach ($this->getInstancesFromFormId($form_id) as $id => $instance) {
        if (isset($form_instances[$id])) {
          continue;
        }
        $form_instances[$id] = $instance;
      }
    }
    return $form_instances;
  }

  /**
   * {@inheritDoc}
   */
  public function alterForm(&$form, FormStateInterface $form_state, $form_id) {
    $executable_instances = [];
    $form_instances = $this->getFormInstances($form, $form_state);
    // Sort instances by execution order.
    foreach ($form_instances as $id => $instance) {
      $definition = $instance->getPluginDefinition();
      $execution_order = $definition['execution_order'];
      $executable_instances[$execution_order][$id] = $instance;
    }
    ksort($executable_instances);
    // Execute instances by execution order.
    foreach ($executable_instances as $list) {
      foreach ($list as $instance) {
        $instance->execute($form, $form_state, $form_id);
      }
    }
    // Add the instances to the form state.
    $form_state->set('form_alterer_plugins', $form_instances);
  }

}
