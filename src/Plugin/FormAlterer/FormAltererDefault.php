<?php

namespace Drupal\form_alterer\Plugin\FormAlterer;

/**
 * The default form_alterer plugin class for yaml plugins with static overrides.
 */
class FormAltererDefault extends FormAltererPluginBase {}
